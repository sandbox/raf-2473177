<?php

/**
 * @file
 * Webform module multiple select component.
 */

/**
 * Implements _webform_defaults_component().
 */
function _webform_defaults_event_session() {
  return array(
    'name' => '',
    'form_key' => NULL,
    'required' => 0,
    'pid' => 0,
    'weight' => 0,
    'value' => '',
    'extra' => array(
      'items' => array(),
      'title_display' => 0,
      'description' => '',
      'private' => FALSE,
      'analysis' => TRUE,
      'required_sessions_disabled' => TRUE,
    ),
  );
}

/**
 * Implements _webform_theme_component().
 */
function _webform_theme_event_session() {
  return array(
    'webform_display_event_session' => array(
      'render element' => 'element',
      'file' => 'components/select.inc',
    ),
  );
}

/**
 * Implements _webform_edit_component().
 */
function _webform_edit_event_session($component) {
  $form = array();

  $form['extra']['items'] = array(
    '#type' => 'fieldset',
    '#title' => t('Sessions'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  for ($i = 0; $i < count($component['extra']['items']) + 5; ++$i) {
    $form['extra']['items'][$i] = array(
      '#type' => 'fieldset',
      '#title' =>  isset($component['extra']['items'][$i]['key'])? $component['extra']['items'][$i]['title'] : t('Create a new session'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    $form['extra']['items'][$i]['key'] = array(
      '#type' => 'textfield',
      '#title' => t('Machine name'),
      '#description' => t('Enter a unique machine readable name for this session.'),
      '#default_value' => isset($component['extra']['items'][$i]['key']) ? $component['extra']['items'][$i]['key'] : NULL,
    );

    $form['extra']['items'][$i]['title'] = array(
      '#type' => 'textfield',
      '#title' => t('Title'),
      '#description' => t('Title for this session.'),
      '#default_value' => isset($component['extra']['items'][$i]['title']) ? $component['extra']['items'][$i]['title'] : NULL,
    );

    $form['extra']['items'][$i]['description'] = array(
      '#type' => 'text_format',
      '#title' => t('Description'),
      '#description' => t('Description for this session.'),
      '#default_value' => isset($component['extra']['items'][$i]['description']['value']) ? $component['extra']['items'][$i]['description']['value'] : NULL,
    );

    $form['extra']['items'][$i]['max_registrations'] = array(
      '#type' => 'textfield',
      '#title' => t('Maxmimum registrations'),
      '#description' => t('Maximum amount of registrations allowed for this session. Enter "-1" if the amount of registrations is unlimited.'),
      '#default_value' => isset($component['extra']['items'][$i]['max_registrations']) ? $component['extra']['items'][$i]['max_registrations'] : NULL,
    );

    $form['extra']['items'][$i]['date_from'] = array(
      '#type' => module_exists('date') ? 'date_select' : 'textfield',
      '#date_format' => 'd-m-Y H:i',
      '#title' => t('Start'),
      '#description' => t('Choose the starting date and time for this session.'),
      '#default_value' => isset($component['extra']['items'][$i]['date_from']) && is_numeric($component['extra']['items'][$i]['date_from']) ? date('Y-m-d H:i', $component['extra']['items'][$i]['date_from']) : NULL,
    );

    $form['extra']['items'][$i]['date_to'] = array(
      '#type' => module_exists('date') ? 'date_select' : 'textfield',
      '#date_format' => 'd-m-Y H:i',
      '#title' => t('End'),
      '#description' => t('Choose the ending date and time for this session.'),
      '#default_value' => isset($component['extra']['items'][$i]['date_to']) && is_numeric($component['extra']['items'][$i]['date_to']) ? date('Y-m-d H:i', $component['extra']['items'][$i]['date_to']) : NULL,
    );
  }

  $form['#validate'][] = '_webform_edit_validate_event_session';

  return $form;
}

/**
 * Validate handler for an event session fields' configuration.
 */
function _webform_edit_validate_event_session($form, &$form_state) {
  $machine_names = array();
  foreach ($form_state['values']['extra']['items'] as $key => $item) {
    // Make sure the machine name includes only lowercase alphanumeric
    // characters and underscores.
    if (isset($form_state['values']['extra']['items'][$key]['key']) && !empty($form_state['values']['extra']['items'][$key]['key'])) {
      $form_element_name = implode('][', $form['extra']['items'][$key]['key']['#array_parents']);
      if (!preg_match('/^[a-z0-9_]+$/i', $form_state['values']['extra']['items'][$key]['key'])) {
        form_set_error(implode('][', $form['extra']['items'][$key]['key']['#array_parents']), t('The %field_key field is invalid. Please include only lowercase alphanumeric characters and underscores.', array('%field_key' => $form['extra']['items'][0]['key']['#title'])));
      }
      else {
        $machine_names[$form_element_name] = $form_state['values']['extra']['items'][$key]['key'];
      }
    }

    // If a field (for a new session) has a value, make sure all required
    // fields are entered correctly. In any other case, make sure the new
    // 'empty' session is not stored
    $copy = $item;
    $copy['description'] = $item['description']['value'];
    $validate = FALSE;
    $errors = array();
    // Find out if there are fields with a value entered.
    foreach (array('key', 'title', 'max_registrations', 'date_from', 'date_to', 'description') as $field_key) {
      if (!empty($copy[$field_key])) {
        $validate = TRUE;
        break;
      }
    }
    // If there are fields with values, find out which required fields
    // do not have a value yet.
    if ($validate) {
      foreach (array('key', 'title', 'max_registrations', 'date_from', 'date_to') as $field_key) {
        if (empty($copy[$field_key])) {
          $errors[] = $field_key;
        }
      }
    }
    // Let the user know one or more required fields we're left empty.
    if (!empty($errors)) {
      foreach ($errors as $field_key) {
        form_set_error(implode('][', $form['extra']['items'][$key][$field_key]['#array_parents']), t('The %name field is required.', array('%name' => $form['extra']['items'][0][$field_key]['#title'])));
      }
    }
    elseif (!$validate) {
      // If a session was completely left empty, make sure it isn't stored.
      unset($form_state['values']['extra']['items'][$key]);
      continue;
    }

    // Convert the entered dates to timestamps.
    foreach (array('date_from', 'date_to') as $date_key) {
      if (isset($item[$date_key]) && !empty($item[$date_key])) {
        $form_state['values']['extra']['items'][$key][$date_key] =  strtotime($item[$date_key]);
      }
    }
  }

  // Check for non-unique machine names.
  foreach (array_count_values($machine_names) as $value => $count) {
    if ($count > 1) {
      $keys = array_keys($machine_names, $value);
      foreach ($keys as $key) {
        form_set_error($key, t('The value for the %name field is not unique.', array('%name' => $form['extra']['items'][0]['key']['#title'])));
      }
    }
  }
}

/**
 * Implements _webform_render_component().
 */
function _webform_render_event_session($component, $value = NULL, $filter = TRUE) {
  if (empty($component['extra']['items'])) {
    return NULL;
  }

  $node = isset($component['nid']) ? node_load($component['nid']) : NULL;

  $element = array(
    '#type' => 'checkboxes',
    '#title' => $filter ? webform_filter_xss($component['name']) : $component['name'],
    '#title_display' => $component['extra']['title_display'] ? $component['extra']['title_display'] : 'before',
    '#required' => $component['required'],
    '#weight' => $component['weight'],
    '#description' => $filter ? webform_filter_descriptions($component['extra']['description'], $node) : $component['extra']['description'],
    '#theme_wrappers' => array('webform_element'),
    '#element_validate'  => array('_webform_validate_event_session'),
    '#pre_render' => array(), // Needed to disable double-wrapping of radios and checkboxes.
    '#options' => array(),
    '#default_value' => is_array($value) ? $value : array(),
  );

  // Render each session as a checkbox.
  $menu_item = menu_get_item();
  $submitted_value = isset($menu_item['page_arguments'][1]->data[$component['cid']]) ? key($menu_item['page_arguments'][1]->data[$component['cid']]) : NULL;
  foreach ($component['extra']['items'] as $item) {
    $element['#options'][$item['key']] = $item['title'];

    // Calculate the registration status.
    $taken_spots = _webform_event_session_count_value($component['nid'], $component['cid'], $item['key']);
    $available_spots = ($item['max_registrations'] > 0) ?  $item['max_registrations'] - $taken_spots : -1;

    // Provide a description for each choice, as well as have it disabled
    // if there are no open spots available (unless we are editing it).
    $output = theme('webform_event_session_field_description', array('display_date_range' => $component['extra']['display_date_range'], 'date_start' => $item['date_from'], 'date_end' => $item['date_to'], 'display_registration_status_available' => $component['extra']['display_registration_status_available'], 'display_registration_status_closed' => $component['extra']['display_registration_status_closed'], 'max_registrations' => $item['max_registrations'], 'available_spots' => $available_spots, 'taken_spots' => $taken_spots, 'description' => $item['description']['value']));
    $element[$item['key']] = array(
      '#description' => $output,
      '#disabled' => $available_spots == 0 && $submitted_value !== $item['key'] ? TRUE : FALSE,
      '#states' => array(),
    );
  }

  // If a session block, with only 1 session, is required - disable the
  // checkbox, have it checked by default and make it non-required.
  if ($element['#required'] && count($element['#options']) === 1) {
    foreach (element_children($element) as $key) {
      $element[$key]['#disabled'] = TRUE;
      $element[$key]['#default_value'] = TRUE;
    }
    $element['#required'] = FALSE;
  }

  return $element;
}

/**
 * Implements _webform_display_component().
 */
function _webform_display_event_session($component, $value, $format = 'html') {
  return array(
    '#title' => $component['name'],
    '#weight' => $component['weight'],
    '#theme' => 'webform_display_event_session',
    '#theme_wrappers' => $format == 'html' ? array('webform_element') : array('webform_element_text'),
    '#format' => $format,
    '#options' => _webform_event_session_options($component),
    '#value' => (array) $value,
  );
}

/**
 * Implements _webform_submit_component().
 *
 * Handle select or other... modifications and convert FAPI 0/1 values into
 * something saveable.
 */
function _webform_submit_event_session($component, $value) {
  if (is_array($value)) {
    $key = key($value);
    if (isset($value[$key]) && $value[$key] === TRUE) {
      $value[$key] = $key;
    }
  }

  return $value;
}

/**
 * Format the text output for this component.
 */
function theme_webform_display_event_session($variables) {
  $element = $variables['element'];

  $output = t('None');
  if (isset($element['#value']) && !empty($element['#value'])) {
    $key = key($element['#value']);
    $output = $element['#options'][$key];
  }

  return $output;
}

/**
 * Implements _webform_analysis_component().
 */
function _webform_analysis_event_session($component, $sids = array()) {
  $options = _webform_event_session_options($component);
  $query = db_select('webform_submitted_data', 'wsd', array('fetch' => PDO::FETCH_ASSOC))
    ->fields('wsd', array('data'))
    ->condition('nid', $component['nid'])
    ->condition('cid', $component['cid'])
    ->condition('data', '', '<>')
    ->condition('data', array_keys($options), 'IN')
    ->groupBy('data');
  $query->addExpression('COUNT(data)', 'datacount');

  if (count($sids)) {
    $query->condition('sid', $sids, 'IN');
  }

  $result = $query->execute()->fetchAllKeyed();
  $rows = array();
  $normal_count = 0;
  foreach ($options as $key => $title) {
    $count = isset($result[$key]) ? $result[$key] : 0;
    $rows[$key] = array(webform_filter_xss($title), $count);
    $normal_count += $count;
    unset($result[$key]);
  }

  foreach ($result as $data => $count) {
    $rows[$data] = $count;
    $normal_count += $count;
  }

  return array(
    'table_rows' => $rows,
  );
}

/**
 * Implements _webform_table_component().
 */
function _webform_table_event_session($component, $value) {
  // Convert submitted 'safe' values to un-edited, original form.
  $options = _webform_event_session_options($component);

  $value = (array) $value;
  $items = array();
  // Set the value as a single string.
  foreach ($value as $option_value) {
    if ($option_value !== '') {
      if (isset($options[$option_value])) {
        $items[] = webform_filter_xss($options[$option_value]);
      }
      else {
        $items[] = check_plain($option_value);
      }
    }
  }

  return implode('<br />', $items);
}

/**
 * Implements _webform_csv_headers_component().
 */
function _webform_csv_headers_event_session($component, $export_options) {
  $headers = array(
    0 => array(),
    1 => array(),
    2 => array(),
  );

  if ($component['extra']['multiple'] && $export_options['select_format'] == 'separate') {
    $headers[0][] = '';
    $headers[1][] = $export_options['header_keys'] ? $component['form_key'] : $component['name'];
    $items = _webform_event_session_options($component);
    $count = 0;
    foreach ($items as $key => $item) {
      // Empty column per sub-field in main header.
      if ($count != 0) {
        $headers[0][] = '';
        $headers[1][] = '';
      }
      if ($export_options['select_keys']) {
        $headers[2][] = $key;
      }
      else {
        $headers[2][] = $item;
      }
      $count++;
    }
  }
  else {
    $headers[0][] = '';
    $headers[1][] = '';
    $headers[2][] = $export_options['header_keys'] ? $component['form_key'] : $component['name'];
  }
  return $headers;
}

/**
 * Implements _webform_csv_data_component().
 */
function _webform_csv_data_event_session($component, $export_options, $value) {
  $options = _webform_event_session_options($component);
  $return = array();

  if ($component['extra']['multiple']) {
    foreach ($options as $key => $item) {
      $index = array_search($key, (array) $value);
      if ($index !== FALSE) {
        if ($export_options['select_format'] == 'separate') {
          $return[] = 'X';
        }
        else {
          $return[] = $export_options['select_keys'] ? $key : $item;
        }
        unset($value[$index]);
      }
      elseif ($export_options['select_format'] == 'separate') {
        $return[] = '';
      }
    }
  }
  else {
    $key = key($value);
    if ($export_options['select_keys']) {
      $return = $key;
    }
    else {
      $return = isset($options[$key]) ? $options[$key] : $key;
    }
  }

  if ($component['extra']['multiple'] && $export_options['select_format'] == 'compact') {
    $return = implode(',', (array) $return);
  }

  return $return;
}

/**
 * Implements _webform_options_component().
 *
 * This function is confusingly an alias of _webform_event_session_options(). However
 * this version is intended to be accessed publicly via
 * webform_component_invoke(), since it is a Webform "hook", rather than an
 * internal, private function. To get the list of select list options for
 * a component, use:
 *
 * @code
 * $options = webform_component_invoke($component['type'], 'options', $component);
 * @endcode
 */
function _webform_options_event_session($component) {
  return _webform_event_session_options($component);
}

/**
 * Generate a list of options for a select list.
 */
function _webform_event_session_options($component) {
  $options = array();

  if (isset($component['extra']['items'])) {
    foreach ($component['extra']['items'] as $item) {
      $options[$item['key']] = $item['title'];
    }
  }

  return $options;
}

/**
 * Counts the amount a given value was entered for a given node and a given
 * component.
 */
function _webform_event_session_count_value($nid, $cid, $value) {
  return db_query('SELECT COUNT(data) FROM {webform_submitted_data} WHERE nid = ? AND cid = ? AND data = ?', array($nid, $cid, $value))->fetchField();
}

/**
 * A Drupal Form API Validation function. Validates the entered values from
 * event session components on the client-side form.
 *
 * @param $form_element
 *   The e-mail form element.
 * @param $form_state
 *   The full form state for the webform.
 * @return
 *   None. Calls a form_set_error if the e-mail is not valid.
 */
function _webform_validate_event_session($form_element, &$form_state) {
  $component = $form_element['#webform_component'];
  $submission = isset($form_state['complete form']['#submission']) ? $form_state['complete form']['#submission'] : NULL;

  if (count($form_element['#value']) > 1) {
    form_error($form_element, t('Only 1 session per session block is allowed.'));
  }
  elseif (!empty($form_element['#value'])) {
    $value = key($form_element['#value']);
    // Get the selected session.
    $item = NULL;
    foreach ($component['extra']['items'] as $item) {
      if ($item['key'] === $value) {
        break;
      }
    }

    // Check if a session without any available spots has been selected. Skip
    // validation if an already edited submission is being edited, and the
    // selected session is unchanged.
    $cid = $form_element['#webform_component']['cid'];
    if ((!isset($submission->data[$cid]) || key($submission->data[$cid]) !== $value) && ($item && $item['max_registrations'] > 0)) {
      $registrations = _webform_event_session_count_value($component['nid'], $component['cid'], $item['key']);
      if ($registrations >= $item['max_registrations']) {
        form_error($form_element, t('%session_title has no available open spots left. Please choose another session.', array('%session_title' => $item['title'])));
      }
    }
  }

  // Remove unselected values from the form_state array.
  $session_block_key = $form_element['#webform_component']['form_key'];
  if (isset($form_state['session_blocks'][$session_block_key]['parents'])) {
    $value = &$form_state['values']['submitted'];
    foreach ($form_state['session_blocks'][$session_block_key]['parents'] as $parent) {
      if (isset($value[$parent])) {
        $value = &$value[$parent];
      }
    }

    $value = array_filter($value);
  }
}
