<?php

/**
 * Returns HTML for a webform event session field.
 */
function theme_webform_event_session_field_description($variables) {
  $output  = '';

  // If configured, display the date range.
  if ($variables['display_date_range'] && $variables['date_start'] && $variables['date_end']) {
    $date_output = t($variables['display_date_range']);
    $tokens = webform_event_session_field_token_scan($date_output);

    if (!empty($tokens)) {
      foreach ($tokens as $type => $items) {
        if ($type !== 'start' && $type !== 'end') {
          continue;
        }

        $timestamp = $variables['date_' . $type];
        foreach ($items as $date_format => $token) {
          $date_output = str_replace($token, date($date_format, $timestamp), $date_output);
        }
      }
    }

    $output .= '<div class="date">';
    $output .= $date_output;
    $output .= '</div>';
  }

  // If configured, display a description.
  if ($variables['description']) {
    $output .= '<div class="text">';
    $output .=   $variables['description'];
    $output .= '</div>';
  }

  // If configured, display the registration status.
  $status = $variables['max_registrations'] < 0 ? 'unlimited' : ($variables['available_spots'] > 0 ? 'available' : 'closed');

  if ($variables['display_registration_status_' . $status]) {
    $status_output = t($variables['display_registration_status_' . $status]);
    $tokens = webform_event_session_field_token_scan($status_output);

    if (!empty($tokens)) {
      foreach ($tokens as $type => $items) {
        if ($type !== 'status') {
          continue;
        }

        foreach ($items as $status_token => $token) {
          $status_key = str_replace('-', '_', $status_token);
          if (isset($variables[$status_key])) {
            $status_output = str_replace($token, $variables[$status_key], $status_output);
          }
        }
      }
    }

    $output .= '<div class="registration-status ' . $status . '">';
    $output .= $status_output;
    $output .= '</div>';
  }

  return $output;
}

/**
 * Builds a list of all token-like patterns that appear in the text.
 *
 * @param $text
 *   The text to be scanned for possible tokens.
 *
 * @return
 *   An associative array of discovered tokens, grouped by type.
 */
function webform_event_session_field_token_scan($text) {
  // Matches tokens with the following pattern: [$type:$name]
  // $type and $name may not contain  [ ] characters.
  // $type may not contain : or whitespace characters, but $name may.
  preg_match_all('/
    \[             # [ - pattern start
    ([^\s\[\]:]*)  # match $type not containing whitespace : [ or ]
    :              # : - separator
    ([^\[\]]*)     # match $name not containing [ or ]
    \]             # ] - pattern end
    /x', $text, $matches);

  $types = $matches[1];
  $tokens = $matches[2];

  // Iterate through the matches, building an associative array containing
  // $tokens grouped by $types, pointing to the version of the token found in
  // the source text. For example, $results['node']['title'] = '[node:title]';
  $results = array();
  for ($i = 0; $i < count($tokens); $i++) {
    $results[$types[$i]][$tokens[$i]] = $matches[0][$i];
  }

  return $results;
}
